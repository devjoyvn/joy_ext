async function chapters(bookUrl) {
    const res = await Extension.request(bookUrl)
    if (!res) return Response.error("The system is under maintenance, please try again later");

    const chaptersEl = await Extension.querySelectorAll(res, 'li.item')
    const chapters = [];
    const host = 'https://mangafire.to'

    for (const el of chaptersEl) {
        const html = el.content;
        const url = await Extension.getAttributeText(html, 'a', 'href');
        const name = await Extension.querySelector(html, 'a > span').text
        chapters.push({
            name: name.replace(/(?:\r\n|\r|\n)/g, '').replace(/ /g, '').trim(),
            url: url.replace(host, ''),
            host,
        })
    }

    return Response.success(chapters.reverse())
}

// runFn(() => chapters('https://mangafire.to/manga/nishuume-boukensha-wa-kakushi-class-juuryoku-tsukai-de-saikyou-omezasu.yvq31'))

async function genre(url) {
    const res = await Extension.request(url);
    if (!res) return Response.error("The system is under maintenance, please try again later");

    const listEl = await Extension.querySelectorAll(res, '#nav-menu>ul>li');
    const listGenresEl = await Extension.querySelectorAll(listEl[1].content, "ul li a")
    let result = [];
    const host = "https://mangafire.to"
    for (const element of listGenresEl) {
        var title = await Extension.querySelector(element.content, 'a').text;
        result.push({
            title:title.trim().replace(/(?:\r\n|\r|\n)/g, '').replace(/ /g, ''),
            url: host + (await Extension.getAttributeText(element.content, 'a', 'href'))
        })
    }

    return Response.success(result);
}

// runFn(() => genre("https://mangafire.to"))

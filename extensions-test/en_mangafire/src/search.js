async function search(url, kw, page) {

    url = url + `/filter?keyword=${kw}&language%5B%5D=en&sort=recently_updated`
    url = url + `&page=${page ?? 1}`
    
    console.log(url)

    const res = await Extension.request(url);
  
    if (!res) return Response.error("The system is under maintenance, please try again later");

    const listEl = await Extension.querySelectorAll(res, '.original .unit')
    const host = 'https://mangafire.to'
    let result = [];
    for (const el of listEl) {
        const html = el.content;
        let link  = await Extension.getAttributeText(html, 'div.inner a', 'href')
        result.push({
            name: await Extension.querySelector(html, 'div.info a').text,
            link: link.replace(host, ''),
            cover: await Extension.getAttributeText(html, 'div.inner a img', 'src'),
            host,
            description: ''
        })
    }

    return Response.success(result);
}

// runFn(() => search("https://mangafire.to", '2', 1))

async function chapter(url) {
  
    const host = "https://mangafire.to"
    const path = url.replace(host, "")
    
    const regex = /\.(.*?)\//;
    const match = path.match(regex);
    let codeBook = ""
    if (match) {
      codeBook = match[1]
    }
  
    const bodyChapters = await Extension.request(`https://mangafire.to/ajax/read/${codeBook}/chapter/en`);
    
    if(bodyChapters.status != 200) {
      return Response.error("The system is under maintenance, please try again later");
    }
    
    let regexDataId = new RegExp(`<a[^>]*href="${path}"[^>]*data-id="([^"]+)"`, 'i');
    let matchDataId = bodyChapters.result.html.match(regexDataId);
    let dataId = ""
    if (matchDataId && matchDataId[1]) {
      dataId = matchDataId[1]
    }
    
    if (dataId == "") {
      return Response.error("The system is under maintenance, please try again later");
    }
    
    const bodyImgChapter = await Extension.request(`https://mangafire.to/ajax/read/chapter/${dataId}`)

    if (bodyImgChapter.status  != 200) {
      return Response.error("The system is under maintenance, please try again later");
    }
    const result = bodyImgChapter.result.images.map(x => x[0])

    return Response.success(result)
}

// runFn(() => chapter("https://mangafire.to/read/nishuume-boukensha-wa-kakushi-class-juuryoku-tsukai-de-saikyou-omezasu.yvq31/en/chapter-78"))

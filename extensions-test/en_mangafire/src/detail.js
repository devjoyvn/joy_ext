async function detail(url) {
    const res = await Extension.request(url);
    if (!res) return Response.error("The system is under maintenance, please try again later");

    const host = "https://mangafire.to";

    const name = await Extension.querySelector(res, 'h1').text;

    const cover = await Extension.getAttributeText(res, '.main-inner>.content>.poster>div>img', 'src');

    const status = await Extension.querySelector(res, '.main-inner>.content>.info>p').text

    let author = [];
    const authorEl = await Extension.querySelectorAll(res, '.main-inner>.sidebar .meta div')
    
    author.push(await Extension.querySelector(authorEl[0].content, 'a').text)

    let description = "" 
   const descriptionDialogEl = await Extension.querySelector(res, "#synopsis")
    if (descriptionDialogEl != null) {
      description = await Extension.querySelector(res, "#synopsis").text
    } else {
      description = await Extension.querySelector(res, ".main-inner>.content>.info h6").text
    }
    // const description = await Extension.querySelector(res, ".summary__content").text

    const genresEl = await Extension.querySelectorAll(authorEl[2].content, 'a');
    console.log(authorEl[2])
    const totalChapters = (await Extension.querySelectorAll(res, 'li.item'))
    console.log(totalChapters)
    const genres = [];

    for (const el of genresEl) {
        const html = el.content;
        const link = host + await Extension.getAttributeText(html, 'a', 'href');
        genres.push({
            url: link,
            title: await Extension.querySelector(html, 'a').text
        })
    }

    return Response.success({
        name: name.trim(),
        cover,
        bookStatus: status.trim(),
        author: author.join(', '),
        description: description.trim(),
        totalChapters: totalChapters.length,
        genres,
        link: url.replace(host, ''),
        host,
    })
}

// runFn(() => detail("https://mangafire.to/manga/nishuume-boukensha-wa-kakushi-class-juuryoku-tsukai-de-saikyou-omezasu.yvq31"))

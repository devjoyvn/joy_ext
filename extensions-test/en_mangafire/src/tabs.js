async function tabs() {
    return Response.success([
        {
            title: "Newest",
            url: "/filter?keyword=&language%5B%5D=en&sort=release_date",
        },
        {
            title: "Added",
            url: "/filter?keyword=&language%5B%5D=en&sort=recently_added"
        },
        {
            title: "Trending",
            url: "/filter?keyword=&language%5B%5D=en&sort=trending"
        },
        {
            title: "Most Viewed",
            url: "/filter?keyword=&language%5B%5D=en&sort=most_viewed"
        },
    ])
}

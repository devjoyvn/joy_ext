async function chapter(url) {
    const res = await Extension.request(url)
    if (!res) return Response.error("O sistema está em manutenção, tente novamente mais tarde");

    const imgEl = await Extension.querySelectorAll(res, '.reading-content img')

    const result = []

    for (const el of imgEl) {
        let i = await Extension.getAttributeText(el.content, 'img', 'src');
        result.push(i.trim())
    }

    return Response.success(result)
}

// runFn(() => chapter("https://flowermanga.net/manga/paciencia-senhorita/capitulo-69/"))

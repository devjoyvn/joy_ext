async function chapters(bookUrl) {
    const res = await Extension.request(bookUrl)
    if (!res) return Response.error("O sistema está em manutenção, tente novamente mais tarde");

    const chaptersEl = await Extension.querySelectorAll(res, '.wp-manga-chapter')
    const chapters = [];
    const host = 'https://flowermanga.net'

    for (const el of chaptersEl) {
        const html = el.content;
        const url = await Extension.getAttributeText(html, 'a', 'href');
        const name = await Extension.querySelector(html, 'a').text
        chapters.push({
            name: name.replace(/(?:\r\n|\r|\n)/g, '').replace(/ /g, '').trim(),
            url: url.replace(host, ''),
            host,
        })
    }

    return Response.success(chapters.reverse())
}

// runFn(() => chapters('https://flowermanga.net/manga/the-lady-wants-to-rest'))

async function detail(url) {
    const res = await Extension.request(url);
    if (!res) return Response.error("O sistema está em manutenção, tente novamente mais tarde");

    const host = "https://flowermanga.net";

    const name = await Extension.querySelector(res, 'h1').text;

    const cover = await Extension.getAttributeText(res, '.summary_image img', 'src');

    const status = await Extension.querySelector(res, 'div.post-status > div:nth-child(2) > div.summary-content').text

    let author = [];
    const authorEls = await Extension.querySelectorAll(res, '.author-content a')
    for (const el of authorEls) {
        author.push(await Extension.querySelector(el.content, 'a').text)
    }

    const description = await Extension.querySelector(res, ".summary__content").text

    const genresEl = await Extension.querySelectorAll(res, '.genres-content a');
    const totalChapters = (await Extension.querySelectorAll(res, '.wp-manga-chapter')).length
    const genres = [];

    for (const el of genresEl) {
        const html = el.content;
        const link = await Extension.getAttributeText(html, 'a', 'href');
        genres.push({
            url: link,
            title: await Extension.querySelector(html, 'a').text
        })
    }

    return Response.success({
        name: name.trim(),
        cover,
        bookStatus: status.trim(),
        author: author.join(', '),
        description: description.trim(),
        totalChapters: totalChapters,
        genres,
        link: url.replace(host, ''),
        host,
    })
}

// runFn(() => detail("https://flowermanga.net/manga/the-lady-wants-to-rest/"))

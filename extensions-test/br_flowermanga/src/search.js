async function search(url, kw, page) {
    if (page > 1) {
        url =url + `/page/${page}`
    }

    const res = await Extension.request(url, {
        queryParameters: {
            s: kw,
            post_type: "wp-manga"
        }
    })
    if (!res) return Response.error("O sistema está em manutenção, tente novamente mais tarde")

    const list = await Extension.querySelectorAll(res, '.tab-content-wrap .c-tabs-item__content')

    const result = [];
    const host= "https://flowermanga.net"
    for (const el of list ) {
        const html = el.content;
        const cover = await Extension.getAttributeText(html, '.tab-thumb img', 'src');
        const link = await Extension.getAttributeText(html, '.tab-summary .post-title a', 'href');
        result.push({
            name: await Extension.querySelector(html, '.tab-summary .post-title a').text,
            link: link.replace(host, ''),
            cover,
            description: "",
            host
        })
    }

    return Response.success(result);
}

// runFn(() => search("https://flowermanga.net", '22', 2))

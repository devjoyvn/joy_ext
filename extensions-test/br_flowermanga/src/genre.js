async function genre(url) {
    const res = await Extension.request(url + "/manga/?genres_collapse=on");
    if (!res) return Response.error("O sistema está em manutenção, tente novamente mais tarde");

    const listEl = await Extension.querySelectorAll(res, '.genres li');

    let result = [];
    const host = "https://flowermanga.net"
    for (const element of listEl) {
        var title = await Extension.querySelector(element.content, 'a').text;
        result.push({
            title:title.trim().replace(/(?:\r\n|\r|\n)/g, '').replace(/ /g, ''),
            url: (await Extension.getAttributeText(element.content, 'a', 'href'))
        })
    }

    return Response.success(result);
}

// runFn(() => genre("https://flowermanga.net/manga/?genres_collapse=on"))

async function home(url, page) {

    const urlSplit = url.split("?")

    if (!page) page = 1;
    url = urlSplit[0] + `/page/${page}`;
    if (urlSplit.length > 1) {
        url = url + "?" + urlSplit[1]
    }

    const res = await Extension.request(url);
    if (!res) return Response.error("O sistema está em manutenção, tente novamente mais tarde");

    const listEl = await Extension.querySelectorAll(res, '.page-item-detail.manga')
    const host = 'https://flowermanga.net'
    let result = [];
    for (const el of listEl) {
        const html = el.content;
        let link  = await Extension.getAttributeText(html, 'h3 a', 'href')
        result.push({
            name: await Extension.querySelector(html, 'h3 a').text,
            link: link.replace(host, ''),
            cover: await Extension.getAttributeText(html, 'a img', 'src'),
            host,
            description: ''
        })
    }

    return Response.success(result);
}

// runFn(() => home("https://flowermanga.net/manga"))

async function tabs() {
    return Response.success([
        {
            title: "ÚLTIMAS ATUALIZAÇÕES DO MANGÁ",
            url: "/manga/?m_orderby=latest",
        },
        {
            title: "Avaliação",
            url: "/manga/?m_orderby=rating"
        },
        {
            title: "Tendendo",
            url: "/manga/?m_orderby=trending"
        },
        {
            title: "Mais visualizações",
            url: "/manga/?m_orderby=views"
        },
        {
            title: "Novo",
            url: "/manga/?m_orderby=new-manga"
        },
    ])
}

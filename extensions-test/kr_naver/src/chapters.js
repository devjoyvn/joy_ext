async function chapters(url) {
    const idBook = url.split('/')[url.split('/').length - 1];
    let apiChapters = `https://comic.naver.com/api/article/list?titleId=${idBook}`
    const bodyChapter = await Extension.request(apiChapters);

    const chapters = [];

    const firstPage = bodyChapter.articleList.filter(x => !x.charge).map(x => {
        return {
            url: `/webtoon/detail?titleId=${idBook}&no=${x.no}`,
            name: x.subtitle,
            host: "https://comic.naver.com"
        }
    })

    chapters.push(...firstPage);

    const {pageInfo} = bodyChapter;

    if (pageInfo.totalPages > 1) {
        for (let i = 2; i <= pageInfo.totalPages ; i++) {
            let apiChapterForPage = apiChapters + `&page=${i}`;
            const bodyChapterForPage = await Extension.request(apiChapterForPage);
            const chaptersForPage = bodyChapterForPage.articleList.filter(x => !x.charge).map(x => {
                return {
                    url: `/webtoon/detail?titleId=${idBook}&no=${x.no}`,
                    name: x.subtitle,
                    host: "https://comic.naver.com"
                }
            })

            chapters.push(...chaptersForPage);
        }
    }

    return Response.success(chapters.reverse());

}

// runFn(() => chapters("https://comic.naver.com/id/793410"))

async function home(url, page) {
    let apiUrl = "https://comic.naver.com/api/webtoon/titlelist";
    if (!page) page = 1;

    const type = url.split('/')[url.split('/').length - 1];

    switch (type) {
        case 'new':
            apiUrl += '/new?order=update';
            break;
        case 'finish':
            apiUrl += `/finished?order=update&page=${page}`;
            break;
        default:
            apiUrl += `/weekday?week=${type}&order=update`
    }

    if (type !== 'finish' && page > 1) {
        return Response.success([])
    }

    const data = await fetch(apiUrl, {method: 'GET'})
    const {titleList} = await  data.json();
    return Response.success(titleList.map(x => {
        return {
            name: x.titleName,
            link: `/id/${x.titleId}`,
            description: "",
            cover: x.thumbnailUrl,
            host: "https://comic.naver.com"
        }
    }))

}

// runFn(() => home("https://comic.naver.com/new"))

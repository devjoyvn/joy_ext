async function tabs() {
    return Response.success([
        {
            title: "신작",
            url: "/new",
        },
        {
            title: "매일+",
            url: "/dailyPlus"
        },
        {
            title: "완결",
            url: "/finish"
        },
        {
            title: "월",
            url: "/mon"
        },
        {
            title: "화",
            url: "/tue"
        },
        {
            title: "수",
            url: "/wed"
        },
        {
            title: "목",
            url: "/thu"
        },
        {
            title: "금",
            url: "/fri"
        },
        {
            title: "토",
            url: "/sat"
        },
        {
            title: "일",
            url: "/sun"
        },

    ])
}

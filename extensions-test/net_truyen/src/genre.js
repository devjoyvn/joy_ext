async function genre(url) {
  const res = await Extension.request(url + "/tim-truyen");
  if (!res) return Response.error("Có lỗi khi tải nội dung");
  const listEl = await Extension.querySelectorAll(
    res,
    '.box-category ul li a'
  );

  const host = "https://nettruyenclub.pro";

  let result = [];
  for (const element of listEl) {
    result.push({
      title: await Extension.querySelector(element.content, "a").text,
      url: host + await Extension.getAttributeText(element.content, "a", "href"),
    });
  }
  return Response.success(result);
}


 runFn(() => genre("https://nettruyenclub.pro"));

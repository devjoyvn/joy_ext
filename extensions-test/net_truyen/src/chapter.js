async function chapter(url) {
  const res = await Extension.request(url);
  if (!res) return Response.error("Có lỗi khi tải nội dung");
  const listEl = await Extension.querySelectorAll(res, "section>div>img");
  let result = [];
  for (const element of listEl) {
    var image = await Extension.getAttributeText(element.content, "img", "src");
    if (image == null) {
      image = await Extension.getAttributeText(
        element.content,
        "img",
        "data-original"
      );
    }
    if (image && image.startsWith("/_next")) {
      image = "https://nettruyenclub.pro" + image;
    }
    result.push(image);
  }
  return Response.success(result);
}

runFn(() => chapter('https://nettruyenclub.pro/truyen-tranh/cau-chuyen-nu-sinh-trung-hoc-va-chang-gia-su-chap-1-6/chap-16-tho-lo-end-vol-1/718064'))

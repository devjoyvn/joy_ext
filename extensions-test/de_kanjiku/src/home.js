async function home(url, page) {
    const res = await Extension.request(url);
    if(!res) return Response.error("Das System wird gerade gewartet. Bitte versuchen Sie es später noch einmal");

    const listEl = await Extension.querySelectorAll(res, '.manga_container')

    const result = []
    const host = 'https://kanjiku.net'

    for (const el of listEl) {
        const html = el.content
        let cover = await Extension.getAttributeText(html, 'img', 'src')
        let name = await Extension.querySelector(html, '.manga_title').text
        let link = await Extension.getAttributeText(html, 'a.manga_box', 'href')

        result.push({
            name,
            link,
            cover: host + cover,
            host,
            description: ''
        })
    }

    return Response.success(result)
}

runFn(() => home("https://kanjiku.net/mangas"))
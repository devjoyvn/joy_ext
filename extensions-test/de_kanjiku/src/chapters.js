async function chapters(url) {
    const res =  await Extension.request(url);
    if (!res) return Response.error("Das System wird gerade gewartet. Bitte versuchen Sie es später noch einmal")

    const listEl = await Extension.querySelectorAll(res, '.manga_chapter')
    const chapters = []
    const host = "https://kanjiku.net"

    for (const el of listEl) {
        const html = el.content;
        const url = await Extension.getAttributeText(html, 'a.latest_ch_number', 'href')
        const name = await Extension.querySelector(html, 'h4').text


        let tempUrl = url.split('/')
        tempUrl.pop()
        tempUrl.push('0')

        chapters.push({
            name,
            url: tempUrl.join('/'),
            host
        })
    }

    return Response.success(chapters.reverse())
}

runFn(() => chapters("https://kanjiku.net/mangas/aiki-s"))
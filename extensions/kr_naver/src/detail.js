async function detail(url) {
    const idBook = url.split('/')[url.split('/').length - 1];

    let api = `https://comic.naver.com/api/article/list/info?titleId=${idBook}`;

    const body = await Extension.request(api);

    let apiChapters = `https://comic.naver.com/api/article/list?titleId=${idBook}`
    const bodyChapter = await Extension.request(apiChapters);



    return Response.success({
        name: body.titleName,
        cover: body.thumbnailUrl,
        bookStatus: body.finished ? '완결' : "",
        author: body.communityArtists.map(x => x.name).join(", "),
        description: body.synopsis,
        totalChapters: bodyChapter.totalCount,
        genres: body.curationTagList.map(x => {
            return {
                url: x.urlPath,
                title: x.tagName
            }
        }),
        link: url.replace("https://comic.naver.com", ""),
        host: "https://comic.naver.com"
    })

}

// runFn(() => detail("https://comic.naver.com/id/793410"))

async function search(url, kw, page)  {
    const api = `https://comic.naver.com/api/search/webtoon?keyword=${kw}&page=${page}`

    const res = await Extension.request(api);
    if (!res) return Response.error("The system is under maintenance, please try again later");

    const {searchList} = res;
    return Response.success(searchList.map(x => {
        return {
            name: x.titleName,
            link: `/id/${x.titleId}`,
            description: x.synopsis,
            cover: x.thumbnailUrl,
            host: "https://comic.naver.com"
        }
    }))

}

// runFn(() => search("", "33", 1))

async function chapter(url) {
    const res = await Extension.request(url);

    if (!res) return Response.error("The system is under maintenance, please try again later");

    const listEl = await Extension.querySelectorAll(res, ".wt_viewer img");

    const result = [];
    for (const el of listEl) {
        var img = await Extension.getAttributeText(el.content, "img", "src");
        if (img) {
            result.push(img)
        }
    }

    return Response.success(result);
}

// runFn(() => chapter("https://comic.naver.com/webtoon/detail?titleId=793410&no=32"))

async function detail(url) {
  const res = await Extension.request(url);
  if (!res) return Response.error("Có lỗi khi tải nội dung");

  const host = "https://nettruyenclub.pro";
  
  const name = await Extension.querySelector(
    res,
    "h1"
  ).text;
  var cover = await Extension.getAttributeText(
    res,
    ".object-cover",
    "data-original"
  );
  if (cover == null) {
    cover = await Extension.getAttributeText(res, ".object-cover", "src");
  }
  if (cover && cover.startsWith("/")) {
    cover = host + cover;
  }

  const bookDes = await Extension.querySelectorAll(res, "ul.text-base li")
  const authorRow = await Extension.querySelectorAll(
    bookDes[1].content,
    "li p"
  );
  var author = "";
  if (authorRow.length == 2) {
    author = await Extension.querySelector(authorRow[1].content, "p").text;
  }

  const statusRow = await Extension.querySelectorAll(
    bookDes[2].content,
    "li p"
  );

  var bookStatus = "";
  if (statusRow.length == 2) {
    bookStatus = await Extension.querySelector(statusRow[1].content, "p").text;
  }

  const description = await Extension.querySelector(
    res,
    ".w-full div.relative"
  ).text;

  const totalChapters = (
    await Extension.querySelectorAll(res, ".table-v2-body ul li")
  ).length;

  let genres = [];
  const genresEl = await Extension.querySelectorAll(
    bookDes[3].content,
    "li p"
  );

  if (genresEl.length == 2) {
    const lstElm = await Extension.querySelectorAll(genresEl[1].content, "a");
    for (var el of lstElm) {
      genres.push({
        url: host + await Extension.getAttributeText(el.content, "a", "href"),
        title: await Extension.querySelector(el.content, "a span").text,
      });
    }
  }

  return Response.success({
    name,
    cover,
    bookStatus,
    author,
    description,
    totalChapters,
    genres,
    link: url.replace(host, ""),
    host: host,
  });
}

runFn(() =>
  detail(
    "https://nettruyenclub.pro/truyen-tranh/cau-chuyen-nu-sinh-trung-hoc-va-chang-gia-su-chap-1-6-3514"
  )
);

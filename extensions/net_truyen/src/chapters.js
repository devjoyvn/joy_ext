async function chapters(bookUrl) {
  const res = await Extension.request(bookUrl);
  if (!res) return Response.error("Có lỗi khi lấy danh sách chương");
  const listEl = await Extension.querySelectorAll(res, ".table-v2-body ul li");
  const chapters = [];
  const host = "https://nettruyenclub.pro";
  for (var index = 0; index < listEl.length; index++) {
    const el = listEl[index].content;
    const url = await Extension.getAttributeText(el, "a", "href");
    const name = await Extension.querySelector(el, "a").text;
    chapters.push({
      name,
      url: url.replace(host, ""),
      host,
    });
  }
  return Response.success(chapters.reverse());
}

// runFn(() =>
//   chapters(
//     "https://nettruyenclub.pro/truyen-tranh/cau-chuyen-nu-sinh-trung-hoc-va-chang-gia-su-chap-1-6-3514"
//   )
// );

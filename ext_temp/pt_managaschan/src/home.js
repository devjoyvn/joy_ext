async function home(url, page) {
    if (!page) page = 1;
    url =url + `page/${page}`

    const res = await Extension.request(url, {headers: {Host: "mangaschan.net"}});
    if (!res) return Response.error("O sistema está em manutenção, tente novamente mais tarde");

    const list = await Extension.querySelectorAll(res, '.page .utao');

    const result = [];

    for (const item of list) {
        const html = item.content;
        var cover = await Extension.getAttributeText(html, '.imgu img', 'src');
        var name = await Extension.querySelector(html, '.luf a').text;
        var link = await Extension.getAttributeText(html, '.luf a', 'href')

        result.push({
            name,
            link,
            description: '',
            cover,
            host: "https://mangaschan.net"
        })
    }

    return Response.success(result)
}

runFn(() => home("https://mangaschan.net/ultimas-atualizacoes/", 2))

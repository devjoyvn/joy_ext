async function detail(url) {

    const res = await Extension.request(url);

    if (!res) return Response.error("Lỗi tải nội dung");

    const host = 'https://qmanga.com';

    const name = await Extension.querySelector(res, "h1").text;

    const status = await Extension.querySelector(res, "table tr:nth-child(4)>td:nth-child(2)>div").text

    const totalChapters = await getChapters(res);

    return Response.success({
        name: name ? name.trim() : "",
        cover: '',
        bookStatus: status ? status : 'Ongoing',
        author: '',
        description: '',
        totalChapters,
        genres: [],
        link: url.replace(host, ""),
        host,
    })

}

async function getChapters(el) {
    const totalChapterOnePage = (await Extension.querySelectorAll(el, '.chapters li')).length

    const lstPages = await Extension.querySelectorAll(el, '.pagination li a')

    if (lstPages.length === 1) {
        return totalChapterOnePage;
    }

    const urlLastPage = await Extension.getAttributeText(el, '.pagination li:last-child a', 'href');

    const index  = urlLastPage.indexOf('?')
    const queries = urlLastPage.substring(index + 1).split('&');
    let lastPage = 1;

    queries.forEach((query) => {
        const att = query.split('=');
        if (att.includes('page')) {
            lastPage = +att[1]
        }
    })

    const host = 'https://qmanga.com';

    if (lastPage > 1) {
        const urlLastPage = await Extension.getAttributeText(el, '.pagination li:last-child a', 'href')

        const lastPageEl = await Extension.request(host + urlLastPage);
        const totalChapterLastPage = (await Extension.querySelectorAll(lastPageEl, '.chapters li')).length

        return totalChapterOnePage * (lastPage - 1) + totalChapterLastPage
    } else {
        return totalChapterOnePage
    }
}

runFn(() => detail('https://qmanga.com/manga-demon/apotheosis-elevation-to-the-status-of-a-god-19aa3378-8e6e-4caa-8967-b8b9054cd087'))

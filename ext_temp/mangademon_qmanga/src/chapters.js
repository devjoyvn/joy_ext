async function chapters(bookUrl) {
    const res = await Extension.request(bookUrl);
    if (!res) return Response.error("Lỗi tải nội dung");

    // const chapters = await getListChapters(res);
    const allPages = await getAllPage(res)

    const allChapters = [];

    for (let i = 1; i <= allPages; i++) {
        const page = await Extension.request(`${bookUrl}/chapters?page=${i}`)
        const listChaptersInPage = await getListChapters(page);
        allChapters.push(...listChaptersInPage);
    }

    return Response.success(allChapters);
}

async function getAllPage(el) {
    // const lstPages = await Extension.querySelectorAll(el, '.pagination li a')
    const urlLastPage = await Extension.getAttributeText(el, '.pagination li:last-child a', 'href');

    const index  = urlLastPage.indexOf('?')
    const queries = urlLastPage.substring(index + 1).split('&');
    let lastPage = 1;

    queries.forEach((query) => {
        const att = query.split('=');
        if (att.includes('page')) {
            lastPage = +att[1]
        }
    })

    return lastPage
}

async function getListChapters(el) {
    const lstChaps = await Extension.querySelectorAll(el, '.chapters li a')
    const chapters = [];
    const host = 'https://qmanga.com'
    for (const chapEl of lstChaps) {
        const url = await Extension.getAttributeText(chapEl.content, 'a', 'href');

        const name = await Extension.querySelector(chapEl.content, 'a').text
        chapters.push({
            url,
            name,
            host
        })
    }

    return chapters;
}


runFn(() => chapters("https://qmanga.com/manga-demon/one-punch-man-384a4247-f775-4bad-b3d5-60c4e6a5ad1e"))

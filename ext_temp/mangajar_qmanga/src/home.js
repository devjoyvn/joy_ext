async function home(url, page) {
    if(!page) page = 1;
    url = url + `&page=${page}`;

    const res = await Extension.request(url);
    if (!res) return Response.error("The system is under maintenance, please try again later");

    const lstEl = await Extension.querySelectorAll(res, "form .my-card");
    const result = [];
    var host = "https://qmanga.com"



    for (const item of lstEl) {
        const html = item.content;
        var link = await Extension.getAttributeText(html, '>a', "href")
        result.push({
            name: await Extension.querySelector(html, ".card-content>b>a").text,
            link: link.replace(host, ""),
            cover: getBgImg(await Extension.getAttributeText(html, '.card-image', 'style')),
            host,
            description: ''
        });
    }
    return Response.success(result);
}

function getBgImg(input){
    const regex = /background-image:\s*url\(['"]?([^'"]+)['"]?\);/
    const result = input.match(regex);

    if (result) {
        return result[1];
    }
    return '';
}


// runFn(() => home("https://qmanga.com/comic-newtype",1))

async function tabs() {
    return Response.success([
        {
            title: "Latest release",
            url: "/mangajar?sort_by=updated_at",
        },
        {
            title: "Trending now",
            url: '/mangajar?sort_by=rank'
        }
    ]);
}

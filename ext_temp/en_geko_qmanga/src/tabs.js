async function tabs() {
    return Response.success([
        {
            title: "Latest release",
            url: "/mangageko?sort_by=updated_at",
        },
        {
            title: "Trending now",
            url: '/mangageko?sort_by=rank'
        }
    ]);
}

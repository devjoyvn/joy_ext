async function tabs() {
    return Response.success([
        {
            title: "Latest release",
            url: "/mangadoom?sort_by=updated_at",
        },
        {
            title: "Trending now",
            url: '/mangadoom?sort_by=rank'
        }
    ]);
}

async function chapter(url) {
    const res = await Extension.request(url + '/pages');

    if (!res) return Response.error("The system is under maintenance, please try again later");

    const listEl = await Extension.querySelectorAll(res, 'img.img-responsive')

    const result = [];
    for (const el of listEl) {
        var img = await Extension.getAttributeText(el.content, "img", 'src');
        if (img == null) {
            img = await Extension.getAttributeText(el.content, 'img', 'data-fallback-src')
        }
        if (img) {
            result.push(img);
        }
    }

    return Response.success(result);
}

// runFn(() => chapter("https://qmanga.com/comic-newtype/%E3%82%B9%E3%83%BC%E3%83%91%E3%83%BC%E3%82%AB%E3%83%96rei/64658"))

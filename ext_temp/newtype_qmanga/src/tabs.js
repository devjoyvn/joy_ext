async function tabs() {
    return Response.success([
        {
            title: "最新のリリース",
            url: "/comic-newtype?sort_by=updated_at",
        },
        {
            title: "今流行っている",
            url: '/comic-newtype?sort_by=rank'
        }
    ]);
}

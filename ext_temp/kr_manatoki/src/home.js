async function home(url, page) {
    if (page > 1) {
        const split = url.split('?')
        const base = split[0]+ `/p${page}`;
        url = base + split[1]
    }

    const res = await Extension.request(url);
    if (!res) return Response.error("시스템 점검 중입니다. 나중에 다시 시도해 주세요.")

    const listEl = await Extension.querySelectorAll(res, '#webtoon-list-all li')
    const host = 'https://manatoki321.net'
    const result = [];

    for (const el of listEl) {
        const html = el.content;
        let link = await Extension.getAttributeText(html, '.img-item>a', 'href')
        let name = await Extension.querySelector(html, '.img-item div a span').text
        let cover = await Extension.getAttributeText(html, '.img-item a img', 'src')

        result.push({
            name,
            link: link.replace(host, ''),
            cover,
            host,
            description: ''
        })
    }

    return Response.success(result)
}

// runFn(() => home("https://manatoki321.net/comic"))

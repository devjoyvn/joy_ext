async function genre(url) {
    const res = await Extension.request(url + '/comic');
    if (!res) return Response.error("시스템 점검 중입니다. 나중에 다시 시도해 주세요.")

    const listEl = await Extension.querySelectorAll(res, '.s-genre:not(.s-all)')

    const result = []
    const host = 'https://manatoki321.net'

    for (const el of listEl) {
        var title = await Extension.querySelector(el.content, 'span').text;
        var value = await Extension.getAttributeText(el.content, 'span', 'data-value')
        result.push({
            title,
            url: `${host}/comic?tag=${value}`
        })
    }

    return Response.success(result);
}

// runFn(() => genre('https://manatoki321.net'))

async function chapters(url) {
    const res = await Extension.request(url);
    if (!res) return Response.error("시스템 점검 중입니다. 나중에 다시 시도해 주세요.")

    const listEl = await Extension.querySelectorAll(res, '.list-body li')

    const chapters = [];

    const host = "https://manatoki321.net"

    for(const el of listEl) {
        const html = el.content;
        const url = await Extension.getAttributeText(html, '.wr-subject a' ,'href' );
        const name = await Extension.querySelector(html, '.wr-subject a').text

        chapters.push({
            name: name.trim().split(' ').filter(x => !!x.trim()).slice(1, -1).join(' '),
            url: url.replace(host, ''),
            host
        })
    }

    return Response.success(chapters);
}

runFn(() => chapters("https://manatoki321.net/comic/124573"))

async function tabs() {
    return Response.success([
        {
            title: "최신 업데이트",
            url: "/comic",
        },
        {
            title: "인기순",
            url: "/comic?sst=as_view"
        },
        {
            title: "추천순",
            url: "/comic?sst=as_good"
        },
    ])
}

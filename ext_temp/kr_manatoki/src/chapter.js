async function chapter(url) {
    const res = await Extension.request(url);
    if (!res) return Response.error("시스템 점검 중입니다. 나중에 다시 시도해 주세요.")

    const imgEl = await Extension.querySelectorAll(res, '#html_encoder_div img')

    const result = [];

    for (const el of imgEl) {
        const img = await Extension.getAttributeText(el.content, 'img', 'src');
        result.push(img)
    }

    return Response.success(result)
}

function decodeHexToHtml(hexString) {
    let hexArray = hexString.split('.');
    let htmlString = '';

    for (let i = 0; i < hexArray.length; i++) {
        let hexValue = hexArray[i];
        let decimalValue = parseInt(hexValue, 16);
        let char = String.fromCharCode(decimalValue);
        htmlString += char;
    }

    return htmlString;
}
runFn(() => chapter("https://manatoki321.net/comic/18116401?spage=1"))

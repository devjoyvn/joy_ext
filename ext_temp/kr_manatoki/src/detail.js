async function detail(url) {
    const res = await Extension.request(url);
    if (!res) return Response.error("시스템 점검 중입니다. 나중에 다시 시도해 주세요.")

    const host = 'https://manatoki321.net'

    const name = await Extension.querySelector(res, 'b').text
    const author = await Extension.querySelector(res, '.col-md-10.col-sm-9 div:nth-child(2) a').text
    const cover = await Extension.getAttributeText(res, '.view-img img', 'src')
    const totalChapters = (await Extension.querySelectorAll(res, '.list-body li')).length

    const tagsEl = await Extension.querySelectorAll(res, '.description tags a');
    const genres = [];

    for (const el of tagsEl) {
        const html = el.content;
        genres.push({
            url: await Extension.getAttributeText(html, 'a', 'href'),
            title: await Extension.querySelector(html, 'a').text
        })
    }

    return Response.success({
        name: name.trim(),
        cover,
        author,
        description: '',
        totalChapters,
        genres,
        link: url.replace(host, ''),
        host
    })

}

runFn(() => detail("https://manatoki321.net/comic/124573"))

async function chapter(url) {
    const res =  await Extension.request(url);
    if (!res) return Response.error("Das System wird gerade gewartet. Bitte versuchen Sie es später noch einmal")

    const imgEl = await Extension.querySelectorAll(res, 'img:not([class])')
    const result = []

    const host = 'https://kanjiku.net'
    for (const el of imgEl) {
        const img = await Extension.getAttributeText(el.content, 'img', 'src')
        result.push(host + img)
    }

    return Response.success(result)
}

runFn(() => chapter("https://kanjiku.net/reader/aiki-s/20_0/0"))
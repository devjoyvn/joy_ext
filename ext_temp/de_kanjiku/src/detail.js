async function detail(url) {
    const res = await Extension.request(url)
    if (!res) return Response.error("Das System wird gerade gewartet. Bitte versuchen Sie es später noch einmal")

    const host = "https://kanjiku.net"

    const name = await Extension.querySelector(res, 'h1.manga_page_title').text
    const author = ''
    const cover = await Extension.getAttributeText(res, '.manga_page_top > img', 'src')
    const totalChapters = (await Extension.querySelectorAll(res, '.manga_chapter')).length
    const genres = [];
    const description = await Extension.querySelector(res, '.manga_description').text

    return Response.success({
        name,
        cover: host + cover,
        author,
        description,
        totalChapters,
        genres,
        link: url.replace(host, ''),
        host
    })
}

runFn(() => detail("https://kanjiku.net/mangas/aiki-s"))